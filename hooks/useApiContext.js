import { useContext } from "react";
import { Context } from "../components/ApiContext";

export default function useApiContext() {
  return useContext(Context);
}