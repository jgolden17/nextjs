import fetch from 'isomorphic-fetch';

export const fetcher = async (query, variables = {}) => {
  const response = await fetch('http://localhost:9095/graphql', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({
      query,
      variables,
    }),
  });

  return response.json();
};
