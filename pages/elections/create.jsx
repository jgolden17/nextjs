import React from 'react';
import { Input, Textarea } from '../../components/bulma';
import Contests from '../../components/Contests';


const CreateElection = () => (
  <div>
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-one-third">
            <h2 className="title is-5 mb-2">Create an election</h2>
            <p className="content">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti optio fugiat repellat vero ex dicta odit, saepe rem eaque. Aut sunt suscipit sint mollitia atque veritatis, autem blanditiis veniam quos.
            </p>
          </div>
          <div className="column">
            <form action="" className="box stack">
              <Input label="Election title" />
              <Textarea label="Description" />
            </form>
          </div>
        </div>
      </div>
    </section>
    <Contests />
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-one-third">
            <h2 className="title is-5 mb-2">Add Proposals</h2>
            <p className="content">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Unde quod quam et esse iste accusamus nisi sunt amet, ipsum aspernatur debitis provident facere at. Voluptas recusandae earum quidem voluptatum dolor.
            </p>
          </div>
          <div className="column">
            <div className="box stack">
              <Input label="Title" />
              <Textarea label="Proposal" />
              <div className="flex flex-justify-end mt-4">
                <button type="button" className="button is-info">Add</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
);

export default CreateElection;
