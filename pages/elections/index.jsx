const elections = [
  {
    title: 'IWW Phoenix GMB General Election 2020',
  },
  {
    title: 'January 2022 General Election',
  },
]

const Elections = () => (
  <div>
    <section className="hero is-small">
      <div className="hero-body">
        <div className="container flex flex-justify-between">
          <p className="title">
            Elections
          </p>
          <a href={`/elections/create`} className="button is-medium is-primary">
            Create election
          </a>
        </div>
      </div>
    </section>
    <section className="container stack">
      <ul className="stack">
        {elections.map((election) => (
          <li key={election.title} className="row">
            <div className="card">
              <div className="card-content">
                <a
                  className="title is-5"
                  src={`/elections/${election.title.split('').map((char) => {
                    if (char === ' ') {
                      return '-';
                    }

                    return char.toLowerCase();
                  }).join('')}`}
                >
                  {election.title}
                </a>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </section>
  </div>
);

export default Elections;
