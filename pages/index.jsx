import useSWR from 'swr';
import Contest from '../components/Contest';
import { fetcher } from '../fetcher';

const Index = (props) => {
  console.log('props', props);
  const { data, error } = useSWR(
    `{
      ballot(id: "61d98b0a9df96a82de99bcd0") {
        id
        title
        status
        contests {
          title
          type
          description
          choices
        }
      }
    }`,
    fetcher
  );

  console.log('data', data);

  if (data && data.data) {
    return (
      <div>
        <div className="container stack">
          <h1 className="title">{data.data.title}</h1>
          {data.data.ballot.contests.map((contest) => {
            const { title, type, description, choices } = contest;

            return (
              <Contest
                key={title}
                type={type}
                title={title}
                description={description}
                choices={choices}
              />
            )
          })}
          <div className="flex flex-justify-end">
            <button className="button is-primary">Submit</button>
          </div>
        </div>
      </div>
    );
  }

  return null;
}

export default Index;
