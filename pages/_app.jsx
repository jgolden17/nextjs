import { Helmet } from 'react-helmet';
import Nav from '../components/Nav';
import '../styles/globals.scss'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Helmet>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
        <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossOrigin="anonymous" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <body className="min-height-full has-background-white-ter" />
      </Helmet>
      <Nav />
      <Component />
    </>
  );
}

export default MyApp
