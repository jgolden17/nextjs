import React, { useState } from 'react';
import Input from '../components/bulma/Input';
import Textarea from '../components/bulma/Textarea';
import NewContest from '../components/NewContest';
import ContestForm from '../components/ContestForm';

const CreateBallot = () => {
  const [contests, setContests] = useState([]);
  const [showNewContest, setShowNewContest] = useState(false);

  const handleSave = (contest) => {
    setContests((previousContests) => [...previousContests, contest]);
    setShowNewContest(false);
  };

  return (
    <div className="container">
      <form className="stack" action="">
        <Input
          label="Election Title"
          id="election-title"
          name="title"
        />
        <Textarea
          label="Election Description"
          id="election-description"
          name="description"
        />
        <section className="section stack is-primary  ">
          <h2 className="title is-4">Contests</h2>
          {contests.map((contest) => (
          <ContestForm
            key={contest.title}
            {...contest}
          />
        ))}
        {showNewContest && (
          <NewContest
            onSave={handleSave}
            onDiscard={() => setShowNewContest(false)}
          />
        )}
        {!showNewContest && <button type="button" className="button is-primary" onClick={() => setShowNewContest(true)}>Add Contest +</button>}
        </section>
      </form>
    </div>
  );
}

export default CreateBallot;
