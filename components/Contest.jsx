import React from 'react';
import RankedEnhanced from './RankedEnhanced';
import RankedSimple from './RankedSimple';
import Approval from './Approval';
import Plurality from './Plurality';

const ContestTypes = {
  RankedEnhanced,
  RankedSimple,
  Approval,
  Plurality,
};

const Contest = ({
  type,
  title,
  description,
  choices,
}) => {
  const Component = ContestTypes[type];

  return (
    <div className="card" key={title}>
      <header className="card-header">
        <h2 className="card-header-title title is-4">
          {title}
        </h2>
      </header>
      <div className="card-content">
        <p className="content">{description}</p>
        {Component && <Component title={title} choices={choices} />}
      </div>
    </div>
  );
};

export default Contest;
