import React, { useState } from 'react';

const RankedEnhanced = ({ choices, title }) => {
  const [selection, setSelection] = useState(new Array(choices.length).fill(null));

  const handleAdd = (choice) => setSelection((previousSelection) => {
    const nextIndex = previousSelection.findIndex((value) => !value);

    const nextSelection = [...selection];

    nextSelection[nextIndex] = choice;

    return nextSelection;
  });

  const handleRemove = (choice) => setSelection((previousSelection) => {
    const index = previousSelection.findIndex((value) => value === choice);

    const nextSelection = [...previousSelection];

    nextSelection.splice(index, 1);

    nextSelection.push(null);

    return nextSelection;
  });

  const handeMove = (choice, direction) => setSelection((previousSelection) => {
    const index = previousSelection.findIndex((value) => value === choice);

    const nextSelection = [...previousSelection];

    nextSelection.splice(index, 1);

    console.log('next', nextSelection);

    nextSelection.splice(index + direction, 0, choice);

    console.log('next', nextSelection);

    return nextSelection;
  });

  return (
    <>
      <p className="content">
        <strong>Instructions:</strong> Use the buttons to add choices to the ballot, and then drag to arrange them in order of preference with your most preferred at the top and your least preferred at the bottom.
      </p>
      <div className="columns">
        <div className="column is-one-third">
          <p>Choices:</p>
          <ul>
            {choices.map((choice) => (
              <li className="flex flex-align-center stack-r" key={`${title}-chioces-${choice}`}>
                <button
                  className="button is-small"
                  onClick={() => handleAdd(choice)}
                  disabled={selection.includes(choice)}
                >
                  Add
                </button>
                <p>{choice}</p>
              </li>
            ))}
          </ul>
        </div>
        <div className="column">
          <p>Your ballot:</p>
          <ol>
            {selection.map((choice, index, array) => (
              <li key={`${title}-ballot-${index.toString()}`}>
                {choice && (
                  <div className="flex flex-align-center stack-r">
                    {choice && (
                      <div className="buttons has-addons margin-b-0">
                        <button
                          className="button is-small margin-b-0"
                          onClick={() => handeMove(choice, -1)}
                          disabled={index === 0}
                        >
                          Move up
                        </button>
                        <button
                          className="button is-small margin-b-0"
                          onClick={() => handeMove(choice, 1)}
                          disabled={index === selection.length - 1 && !array[index + 1]}
                        >
                          Move down
                        </button>
                      </div>
                    )}
                    <p>{choice}</p>
                    {choice && (
                      <button
                        className="button is-small margin-b-0"
                        onClick={() => handleRemove(choice)}
                      >
                        Remove
                      </button>
                    )}
                  </div>
                )}
              </li>
            ))}
          </ol>
        </div>
      </div>
    </>
  );
};

export default RankedEnhanced;
