import React, { useState } from 'react';

const Plurality = ({ choices, title }) => {
  return (
    <>
      <p className="content">
        <strong>Instructions:</strong> Use the buttons to add choices to the ballot, and then drag to arrange them in order of preference with your most preferred at the top and your least preferred at the bottom.
      </p>
      <div>
        <p>Your ballot:</p>
        <ul>
          {choices.map((choice, index) => (
            <li key={`${title}-ballot-${index.toString()}`}>
              <div className="flex flex-align-center stack-r">
                <label className="radio">
                  <input type="radio" name="answer" />
                  <span>{choice}</span>
                </label>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default Plurality;
