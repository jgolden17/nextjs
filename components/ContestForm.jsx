import React, { useState } from "react";
import { Input, Select, Textarea } from './bulma';

const ContestForm = ({
  title = '',
  description = '',
  type,
  options,
  winnerCount = 1,
  onSave,
  onDiscard,
}) => {
  const initialState = {
    title: '',
    options: '',
    contestType: 'Plurality',
    winnerCount: 1,
  };

  const [state, setState] = useState({ ...initialState });

  const handleChange = (e) => {
    const { name, value } = e.target;

    setState({
      ...state,
      [name]: value,
    });
  };

  const handleSave = () => {
    onSave(state);
    setState({ ...initialState });
  };

  console.log('state', state);

  return (
    <form action="" className="form">
      <Input
        label="Title"
        name="title"
        id="contest-title"
        onChange={handleChange}
        value={state.title}
      />
      <Textarea
        label="Options"
        name="options"
        id="contest-options"
        placeholder={options}
        onChange={handleChange}
        value={state.options}
      />
      <Select
        label="Contest Type"
        name="contestType"
        id="contestType"
        options={[
          { value: 'Plurality', label: 'Plurality' },
          { value: 'Approval', label: 'Approval' },
          { value: 'RankedSimple', label: 'Simple Ranked' },
          { value: 'RankedEnhanced', label: 'Enhanced Ranked' },
        ]}
        value={state.contestType}
        onChange={handleChange}
      />
      <Input
        type="number"
        label="Number of winners"
        name="winnerCount"
        id="contest-winner-count"
        defaultValue={1}
        onChange={handleChange}
        value={state.winnerCount}
      />
      <div className="flex flex-justify-end mt-4">
        <button
          type="button"
          className="button is-info"
          onClick={handleSave}
        >
          Add
        </button>
      </div>
    </form>
  );
};

export default ContestForm;
