import React from 'react';

const Input = ({
  id,
  label,
  type = 'text',
  value,
  ...rest
}) => (
  <article className="field">
    <label htmlFor={id} className="label">{label}</label>
    <p className="control">
      <input
        id={id}
        className="input"
        type={type}
        value={value}
        {...rest}
      />
    </p>
  </article>
);

export default Input;
