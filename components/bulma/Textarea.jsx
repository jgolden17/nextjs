import React from 'react';

const Textarea = ({
  id,
  label,
  ...rest
}) => (
  <div className="field">
    <label htmlFor={id} className="label">{label}</label>
    <p className="control">
      <textarea
        id={id}
        className="textarea"
        {...rest}
      />
     </p>
  </div>
);

export default Textarea;
