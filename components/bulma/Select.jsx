const Select = ({ id, name, label, options, ...rest }) => (
  <article className="field">
    <label className="label" htmlFor={id}>{label}</label>
    <p className="control">
      <span className="select">
        <select id={id} name={name} type="select" {...rest}>
          {options.map((option) => (
            <option key={option.value} value={option.value}>{option.label}</option>
          ))}
        </select>
      </span>
    </p>
  </article>
);

export default Select;
