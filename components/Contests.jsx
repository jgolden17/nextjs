import { useState } from "react"
import ContestForm from "./ContestForm";

const Contests = () => {
  const [contests, setContests] = useState([]);
  const [showForm, setShowForm] = useState(contests.length === 0);

  const handleAdd = (contest) => setContests([...contests, contest]);

  return (
    <section className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-one-third">
            <h2 className="title is-5 mb-2">Add Contests</h2>
            <p className="content">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Incidunt quas aspernatur at, perspiciatis beatae a fugiat nostrum et sint repellat illum quaerat, eum atque nobis provident quam doloremque eaque quisquam.
            </p>
          </div>
          <div className="column stack">
            <div className="stack">
              {contests.map((contest) => (
                <details key={contest.title} className="box">
                  <summary className="title is-6 m-0">{contest.title}</summary>
                  <div className="p-4 stack">
                    <p className="content">
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Optio consectetur hic quis, vero ipsum harum pariatur neque placeat, accusamus ab necessitatibus ipsa veritatis, assumenda impedit iure blanditiis. Consequuntur, alias omnis.
                    </p>
                    <article>
                      <p id={`${contest.title}-options`} className="label">Options:</p>
                      <ul aria-labelledby={`${contest.title}-options`}>
                        {contest.options.split('\n').map((option) => (
                          <li key={`${contest.title}-${option}`}>{option}</li>
                        ))}
                      </ul>
                    </article>
                    <article>
                      <p className="label">Contest Type:</p>
                      <span className="tag is-light is-medium">
                        {contest.contestType}
                      </span>
                    </article>
                    <article>
                      <p className="label">Possible Winners</p>
                      <span>{contest.winnerCount}</span>
                    </article>
                  </div>
                </details>
              ))}
            </div>
            {showForm && (
              <div className="box stack">
                <ContestForm
                  onSave={handleAdd}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contests;
