import React, { useState } from 'react';

const RankedSimple = ({ choices, title }) => {
  const [selection, setSelection] = useState(choices);

  const handeMove = (choice, direction) => setSelection((previousSelection) => {
    const index = previousSelection.findIndex((value) => value === choice);

    const nextSelection = [...previousSelection];

    nextSelection.splice(index, 1);

    console.log('next', nextSelection);

    nextSelection.splice(index + direction, 0, choice);

    console.log('next', nextSelection);

    return nextSelection;
  });

  return (
    <>
      <p className="content">
        <strong>Instructions:</strong> Use the buttons to add choices to the ballot, and then drag to arrange them in order of preference with your most preferred at the top and your least preferred at the bottom.
      </p>
      <div>
        <p>Your ballot:</p>
        <ol>
          {selection.map((choice, index, array) => (
            <li key={`${title}-ballot-${index.toString()}`}>
              <div className="flex flex-align-center stack-r">
                <div className="buttons has-addons margin-b-0">
                  <button
                    className="button is-small margin-b-0"
                    onClick={() => handeMove(choice, -1)}
                    disabled={index === 0}
                  >
                    Move up
                  </button>
                  <button
                    className="button is-small margin-b-0"
                    onClick={() => handeMove(choice, 1)}
                    disabled={index === selection.length - 1 && !array[index + 1]}
                  >
                    Move down
                  </button>
                </div>
                <p>{choice}</p>
              </div>
            </li>
          ))}
        </ol>
      </div>
    </>
  );
};

export default RankedSimple;
