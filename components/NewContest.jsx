import React, { useState } from 'react';
import Input from './bulma/Input';
import Textarea from './bulma/Textarea';

const NewContest = ({
  title = '',
  description = '',
  options = `Option 1\nOption 2\nOption 3`,
  winnerCount = 1,
  onSave,
  onDiscard,
}) => {
  const [state, setState] = useState({
    title,
    description,
    options,
    winnerCount,
  });

  const handleChange = (e, ...rest) => setState((previousState) => {
    const { name, value } = e.target;

    return {
      ...previousState,
      [name]: value,
    };
  });

  return (
    <div className="card">
      <div className="card-content">
      <Input
        label="Title"
        id="contest-title"
        name="title"
        value={state.title}
        onChange={handleChange}
      />
      <Textarea
        id="contest-description"
        name="description"
        value={state.description}
        label="Description"
        onChange={handleChange}
      />
      <Textarea
        id="contest-options"
        name="options"
        value={state.options}
        label="Options"
        onChange={handleChange}
      />
      <article className="field">
        <label className="label" htmlFor="">Contest Type</label>
        <p className="control">
          <span className="select">
          <select name="type" type="select" onChange={handleChange}>
            <option value=""></option>
            <option value="Plurality">Plurality</option>
            <option value="Approval">Approval</option>
            <option value="Simple Ranked">Simple Ranked</option>
            <option value="Enhanced Ranked">Enhanced Ranked</option>
          </select>
          </span>
        </p>
      </article>
      <Input
        id="contents-winner-count"
        name="winnerCount"
        value={state.winnerCount}
        label="Number of Winners"
        type="number"
        onChange={handleChange}
      />
      </div>
      <footer className="flex pt-4 flex-justify-end">
        <div className="buttons">
          <button
            type="button"
            className="button is-primary"
            onClick={() => onSave(state)}
          >
            Save
          </button>
          <button
            type="button"
            className="button"
            onClick={onDiscard}
          >
            Discard
          </button>
        </div>
      </footer>
    </div>
  );
}

export default NewContest;
